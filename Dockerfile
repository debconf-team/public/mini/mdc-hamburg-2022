FROM debian:bullseye AS base
RUN apt-get update && \
    apt-get install -qy auto-apt-proxy && \
    apt-get install -qy \
        fonts-fork-awesome \
        gettext \
        gunicorn \
        libjs-bootstrap4 \
        libjs-moment \
        libjs-moment-timezone \
        python3-django \
        python3-django-countries \
        python3-pkg-resources \
        python3-six \
        python3-whitenoise \
        python3-yaml \
        wget \
    && echo "Base packages installed"

FROM base AS build
RUN apt-get install -qy \
        git \
        python3-pip \
        rsync \
    && \
    echo "Build packages installed"

# stuff from pypi
COPY requirements-base.txt ./requirements-base.txt
RUN pip3 install -r requirements-base.txt
COPY requirements.txt ./requirements.txt
RUN pip3 install -r requirements.txt && \
    rm -rf /root/.cache

FROM base
WORKDIR /app
COPY --from=build /usr/local/lib /usr/local/lib
COPY --from=build /usr/local/bin /usr/local/bin
COPY . ./

ENV DATADIR=/data
RUN ln -sfT /data/localsettings.py localsettings.py
RUN ./manage.py collectstatic --no-input -v 0
RUN mkdir -p /data/static-cache
RUN ln -sf /data/static-cache localstatic/CACHE
RUN ./manage.py compress --force

USER nobody
CMD ./manage.py migrate && \
    ./manage.py createcachetable && \
    ./manage.py create_debconf_groups && \
    ./manage.py init_minidc_menu_pages && \
    gunicorn --workers=2 --threads=4 --worker-class=gthread wsgi
