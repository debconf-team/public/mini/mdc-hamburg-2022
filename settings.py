from pathlib import Path

from minidebconf.settings import *

basedir = Path(__file__).parent
datadir = Path(os.getenv("DATADIR", str(basedir)))

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': str(datadir / 'wafer.db'),
    }
}


# site description and author. override in local settings
SITE_DESCRIPTION = None
SITE_AUTHOR = None
DEBCONF_ONLINE = True

WAFER_TALKS_OPEN = False
INSTALLED_APPS += (
    'hacks',
)


# local templates
TEMPLATES[0]['DIRS'] = TEMPLATES[0]['DIRS'] + (str(basedir / "templates"),)

# static files
STATICFILES_DIRS = (str(basedir / "static"),)
STATIC_ROOT = str(basedir / 'localstatic')
STATICFILES_STORAGE = 'whitenoise.storage.CompressedManifestStaticFilesStorage'
MIDDLEWARE = list(MIDDLEWARE)
try:
    security_idx = MIDDLEWARE.index("django.middleware.security.SecurityMiddleware")
except ValueError:
    security_idx = 0
MIDDLEWARE.insert(security_idx, 'whitenoise.middleware.WhiteNoiseMiddleware')

# uploads
MEDIA_ROOT = str(datadir / 'localmedia')
ROOT_URLCONF = 'urls'

ALLOWED_HOSTS = [ "*"]
USE_X_FORWARDED_HOST = True
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

__secret_key_file__ = datadir / 'secret.txt'
if __secret_key_file__.exists():
    SECRET_KEY = __secret_key_file__.read_text().strip()
else:
    from django.core.management.utils import get_random_secret_key
    SECRET_KEY = get_random_secret_key()
    if datadir.exists():
        __secret_key_file__.touch(mode=0o600)
        __secret_key_file__.write_text(SECRET_KEY + "\n")


# Log errors to the console
LOGGING['formatters'] = {
    'console_formatter': {
        'class': 'logging.Formatter',
        "format": "[%(asctime)s] [%(process)d] [%(levelname)s] %(message)s",
        "datefmt": "%Y-%m-%d %H:%M:%S %z",
    },
}
LOGGING['handlers']['console'] = {
    'level': 'ERROR',
    'class': 'logging.StreamHandler',
    'formatter': 'console_formatter',
}
LOGGING['loggers']['django.request']['handlers'].append('console')

WAFER_SSO = ('gitlab',)
WAFER_GITLAB_HOSTNAME = 'salsa.debian.org'
# Define these in localsettings.py:
# WAFER_GITLAB_CLIENT_ID = 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'
# WAFER_GITLAB_CLIENT_SECRET = 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'

WAFER_CONFERENCE_ACRONYM = 'debian-reunion-hamburg-2022'
DEBCONF_VENUE_IRC_CHANNELS = ["#debconf-hamburg"]

try:
    from localsettings import *
except ImportError:
    pass
