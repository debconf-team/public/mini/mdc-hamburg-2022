import re
from django.conf.urls import include, url
from django.views.static import serve
from django.conf import settings

from debconf.views import now_or_next


urlpatterns = [
    url(r"", include("minidebconf.urls")),
    url(r'^now_or_next/(?P<venue_id>\d+)/$', now_or_next, name="now_or_next"),
    url(
        r"^%s(?P<path>.*)$" % re.escape(settings.MEDIA_URL.lstrip("/")),
        serve,
        {"document_root": settings.MEDIA_ROOT},
    ),
]
